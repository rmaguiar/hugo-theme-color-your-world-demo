# [Color Your World](https://gitlab.com/rmaguiar/hugo-theme-color-your-world) (demo site)

[![Hugo](https://img.shields.io/badge/Hugo-%5E0.128.0-ff4088?logo=hugo)](https://gohugo.io/)
[![Hugo Themes](https://badgen.net/badge/Hugo%20Themes/Color%20Your%20World?color=1dbc91)](https://themes.gohugo.io/hugo-theme-color-your-world/)
[![Netlify Status](https://api.netlify.com/api/v1/badges/fcc6d88f-4913-4716-be24-1888c3acaa03/deploy-status)](https://app.netlify.com/sites/color-your-world-demo/deploys)

Some metrics for those who care:

* [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fcolor-your-world-demo.netlify.app%2F)
* [Yellow Lab Tools](https://yellowlab.tools/result/ght3sa9o5t)
